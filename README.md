# Java Simple Calculator

This project is making use of ANT and Ivy. To build the project, you need to:

1. Download ANT 1.10.1 from: and unzip it into a directory of your choice
2. Setup environment variable: ANT_HOME to point to the installation directory
3. Add ANT_HOME/bin to your path environment variable
4. Copy the following files from the [git base dir]/lib and put it in ANT_HOME/lib:
    1. ant-contrib.jar
    2.  ivy-2.4.0.jar

## Build

Run `ant` from the git repo base directory

## Run

Run `ant run -Djvm.run.arguments="operand operator operand"` for example:
> ant run -Djvm.run.arguments="1 + 5"

## Testing

The tests are written using JUnit. The test suite is class is: `com.ebarten.calculator.SimpleCalculatorTestSuite`
I leave it up to you to set the execution for it as I find Intellij build in runner the most detailed adn simple to use
rather then a command line report generation
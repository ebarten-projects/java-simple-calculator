package com.ebarten.calculator;

/**
 * Created by Eran on 1/04/2017.
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class uses as a test suite to run all tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        FormulaParserTest.class
})
public class SimpleCalculatorTestSuite {
}

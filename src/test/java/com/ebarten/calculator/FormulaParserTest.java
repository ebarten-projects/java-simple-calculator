package com.ebarten.calculator;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.parse.string.FormulaParser;
import com.ebarten.calculator.parse.string.NumberParser;
import com.ebarten.calculator.parse.string.StringFormulaParser;
import com.ebarten.calculator.parse.string.operators.DivisionParser;
import com.ebarten.calculator.parse.string.operators.MinusParser;
import com.ebarten.calculator.parse.string.operators.MultiplicationParser;
import com.ebarten.calculator.parse.string.operators.PlusParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * The class tests the program arguments format.
 */
public class FormulaParserTest {

    private static FormulaParser _formulaParser = FormulaParser.getInstance();

    @BeforeClass
    public static void setup() {
        StringFormulaParser parser = new NumberParser();
        _formulaParser.addParser(parser);
        parser = new PlusParser();
        _formulaParser.addParser(parser);
        parser = new MinusParser();
        _formulaParser.addParser(parser);
        parser = new MultiplicationParser();
        _formulaParser.addParser(parser);
        parser = new DivisionParser();
        _formulaParser.addParser(parser);
    }

    /**
     * The method tests that the structure of the supplied input falls within the specifications of: operand1, operation, operand2
     */
    @Test
    public void whenInputStructureInvalid() {
        String formula = "*123";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable != null) {
            Assert.fail("Formula should not be accepted as left operand is missing: " + formula);
        }
    }

    /**
     * The method tests that an operand answer the proper syntax. That is: of a number with potentially a decimal point and could be a negative number as well.
     */
    @Test
    public void whenOperandFormatIsIncorrect() {
        String formula = ".12.3+123";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable != null) {
            Assert.fail("Formula should not be accepted. Unknown operator format: " + formula);
        }
    }

    /**
     * The method tests that a number only is accepted as well by the parser.
     */
    @Test
    public void whenOnlyNumberSupplied() {
        String formula = "123";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula should BE accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 123, result, 0);

    }

    /**
     * The method tests that only the acceptable operations are approved by the calculator.
     */
    @Test
    public void whenOperationNotSupported() {
        String formula = "123%123";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable != null) {
            Assert.fail("Formula should not be accepted. Unknown operator format: " + formula);
        }
    }

    /**
     * The method tests formula with a '+' operator calculation.
     */
    @Test
    public void testPlusResult() {
        String formula = "123+123";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula SHOULD be accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 246, result, 0);
    }

    /**
     * The method tests formula with a '-' operator calculation.
     */
    @Test
    public void testSubtractionResult() {
        String formula = "123-2";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula SHOULD be accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 121, result, 0);
    }

    /**
     * The method tests formula with a '*' operator calculation.
     */
    @Test
    public void testMultiplicationResult() {
        String formula = "123*2";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula SHOULD be accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 246, result, 0);
    }

    /**
     * The method tests formula with a '/' operator calculation.
     */
    @Test
    public void testDivisionResult() {
        String formula = "124/2";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula SHOULD be accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 62, result, 0);
    }

    /**
     * The method tests the operators order.
     */
    @Test
    public void testOperatorsOrderResult() {
        String formula = "12+2*4+2/2-1";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula SHOULD be accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 20, result, 0);
    }

    /**
     * The method tests that a formula that contains spaces is a valid formula
     */
    @Test
    public void testFormulaWithSpaces(){
        String formula = " 12 + 2 ";
        Calculable calculable = _formulaParser.parse(formula);
        if (calculable == null) {
            Assert.fail("Formula SHOULD be accepted: " + formula);
        }

        double result = calculable.calculate();
        Assert.assertEquals("Unexpected formula result", 14, result, 0);
    }

}

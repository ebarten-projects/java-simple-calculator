package com.ebarten.calculator.parse.string;

import com.ebarten.calculator.calculables.Calculable;

import java.util.ArrayList;

/**
 * The class is a singleton that is used to parse a formula based on a list of parsers. <BR/>
 * <U><B>NOTE:</B></U>The order of the parsers determines the importance of the operators which will effect the way the
 * formula will be computed. The list should be ordered from least to most important.
 */
public class FormulaParser implements StringFormulaParser {

    private static volatile FormulaParser _instance = new FormulaParser();

    private ArrayList<StringFormulaParser> _parserArrayList = new ArrayList<StringFormulaParser>();

    /**
     * The method returns the instance of this singleton.
     * @return The instance of this singleton.
     */
    public static FormulaParser getInstance(){
        return _instance;
    }

    @Override
    public Calculable parse(String formula) {
        Calculable result = null;

        for (StringFormulaParser currParser : _parserArrayList) {
            result = currParser.parse(formula);
            if (result != null) {
                break;
            }
        }

        return result;
    }

    /**
     * The method adds a parser to the list of parsers to be used by this class. <BR/>
     * <U><B>NOTE:</B></U>The order of the parsers determines the importance of the operators which will effect the
     * way the formula will be computed. The list should be ordered from least to most important.
     * @param parser The parser to add.
     */
    public void addParser(StringFormulaParser parser) {
        _parserArrayList.add(parser);
    }

    /**
     * The method adds the passed parser at the specified index to the list of parsers to be used by this class. <BR/>
     * <U><B>NOTE:</B></U>The order of the parsers determines the importance of the operators which will effect the
     * way the formula will be computed. The list should be ordered from least to most important.
     * @param idx The index to add the parse at.
     * @param parser The parser to add.
     */
    public void addParser(int idx, StringFormulaParser parser) {
        _parserArrayList.add(idx, parser);
    }

    /**
     * The method returns the index of the passed parser in the list of parsers.
     * @param parser The parser to get the index for.
     * @return The index of the passed parser in the list of parsers.
     */
    public int indexOf(StringFormulaParser parser){
        int idx = _parserArrayList.indexOf(parser);
        return idx;
    }
}

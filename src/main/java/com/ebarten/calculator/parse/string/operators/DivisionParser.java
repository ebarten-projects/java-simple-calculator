package com.ebarten.calculator.parse.string.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.operators.Division;
import com.ebarten.calculator.calculables.operators.Plus;

/**
 * The class implements a Division operator parser.
 */
public class DivisionParser extends BasicOperatorParser {

    @Override
    protected String getOperator() {
        return "/";
    }

    @Override
    protected Calculable calculableFor(Calculable leftOperand, Calculable rightOperand) {
        return new Division(leftOperand, rightOperand);
    }
}

package com.ebarten.calculator.parse.string.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.Number;
import com.ebarten.calculator.parse.string.FormulaParser;
import com.ebarten.calculator.parse.string.StringFormulaParser;

/**
 * The class defines the basic behaviour for parsing a formula consists of simple operators like: * / + -
 */
public abstract class BasicOperatorParser implements StringFormulaParser {

    private FormulaParser _formulaParser = FormulaParser.getInstance();

    @Override
    public Calculable parse(String formula) {
        Calculable result = null;

        // Get the operator to search the formula for.
        String operator = getOperator();
        int idx = formula.indexOf(operator);
        if (idx > 0) {
            // extract left and right operands
            String leftOperand = formula.substring(0, idx);
            String rightOperand = formula.substring(idx + 1);

            // Parse left and right operands
            Calculable leftCalculable= _formulaParser.parse(leftOperand);
            if (leftCalculable != null) {
                Calculable rightCalculable = _formulaParser.parse(rightOperand);
                result = calculableFor(leftCalculable, rightCalculable);
            }
        }

        return result;
    }

    protected abstract String getOperator();
    protected abstract Calculable calculableFor(Calculable leftOperand, Calculable rightOperand);
}

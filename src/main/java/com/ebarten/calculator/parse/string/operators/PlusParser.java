package com.ebarten.calculator.parse.string.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.operators.Plus;

/**
 * The class implements a plus operator parser.
 */
public class PlusParser extends BasicOperatorParser {

    @Override
    protected String getOperator() {
        return "+";
    }

    @Override
    protected Calculable calculableFor(Calculable leftOperand, Calculable rightOperand) {
        return new Plus(leftOperand, rightOperand);
    }
}

package com.ebarten.calculator.parse.string;

import com.ebarten.calculator.calculables.Calculable;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * The class defines the
 */
public interface StringFormulaParser {

    /**
     * The method parses the passed formula and returns the matching calculable for it. null - if non was found.
     * @param formula The formula to parse.
     * @return he matching calculable for it. null - if non was found.
     */
    public abstract Calculable parse(String formula);

}

package com.ebarten.calculator.parse.string.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.operators.Minus;
import com.ebarten.calculator.calculables.operators.Plus;

/**
 * The class implements a Minus operator parser.
 */
public class MinusParser extends BasicOperatorParser {

    @Override
    protected String getOperator() {
        return "-";
    }

    @Override
    protected Calculable calculableFor(Calculable leftOperand, Calculable rightOperand) {
        return new Minus(leftOperand, rightOperand);
    }
}

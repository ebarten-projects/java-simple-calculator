package com.ebarten.calculator.parse.string;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.Number;

/**
 * The class parses a formula containing a number (with/out decimal points or leading +/-).
 */
public class NumberParser implements StringFormulaParser {

    @Override
    public Calculable parse(String formula) {
        Calculable result = null;

        try {
            double number = Double.valueOf(formula);
            result = new Number(number);
        } catch (NumberFormatException e) {
            // Do nothing - result is already set to false.
        }

        return result;
    }
}

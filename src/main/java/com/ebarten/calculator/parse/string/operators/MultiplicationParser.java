package com.ebarten.calculator.parse.string.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.operators.Multiplication;
import com.ebarten.calculator.calculables.operators.Plus;

/**
 * The class implements a Multiplication operator parser.
 */
public class MultiplicationParser extends BasicOperatorParser {

    @Override
    protected String getOperator() {
        return "*";
    }

    @Override
    protected Calculable calculableFor(Calculable leftOperand, Calculable rightOperand) {
        return new Multiplication(leftOperand, rightOperand);
    }
}

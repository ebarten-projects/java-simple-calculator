package com.ebarten.calculator;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.parse.string.FormulaParser;
import com.ebarten.calculator.parse.string.NumberParser;
import com.ebarten.calculator.parse.string.StringFormulaParser;
import com.ebarten.calculator.parse.string.operators.DivisionParser;
import com.ebarten.calculator.parse.string.operators.MinusParser;
import com.ebarten.calculator.parse.string.operators.MultiplicationParser;
import com.ebarten.calculator.parse.string.operators.PlusParser;

/**
 * The class executes a simple calculator with basic operators.
 */
public class SimpleCalculator {
    public static void main(String[] args) {
        int argsNum = args.length;
        if (argsNum != 3) {
            displayUsage();
            System.exit(1);
        }

        String formula = args[0].trim() + " " + args[1].trim() +" " + args[2].trim();

        FormulaParser formulaParser = initFormulaParser();
        Calculable calculable = formulaParser.parse(formula);
        if (calculable != null) {
            double result = calculable.calculate();
            System.out.println("Formula: " + formula);
            System.out.println("Result: " + result);
        } else {
            System.out.println("Invalid formula: " + formula);
        }

    }

    /**
     * The method initialises the formula parser with basic operations.
     * @return The initialised instance of the parser.
     */
    private static FormulaParser initFormulaParser() {
        FormulaParser formulaParser = FormulaParser.getInstance();
        StringFormulaParser parser = new NumberParser();
        formulaParser.addParser(parser);
        parser = new PlusParser();
        formulaParser.addParser(parser);
        parser = new MinusParser();
        formulaParser.addParser(parser);
        parser = new MultiplicationParser();
        formulaParser.addParser(parser);
        parser = new DivisionParser();
        formulaParser.addParser(parser);
        return formulaParser;
    }

    /**
     * The method displays the programs usage to the the user.
     */
    private static void displayUsage() {
        String usageMessage = "Usage: java -jar simple-calculator.jar [operand] [operator] [operand]\n" +
                "Where: \n" +
                "        operand:   Should follow the format: [0-9]+[.]?[0-9]* \n" +
                "        operator:  Could be one of: * / + or - ";
        System.out.println(usageMessage);
    }
}

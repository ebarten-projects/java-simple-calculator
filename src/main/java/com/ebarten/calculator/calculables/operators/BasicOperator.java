package com.ebarten.calculator.calculables.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.Operation;

/**
 * Created by Eran on 1/04/2017.
 */
public abstract class BasicOperator implements Operation {

    private Calculable _leftOperand;
    private Calculable _rightOperand;

    /**
     * The constructor of this class.
     * @param leftOperand The left operand that this operation works on.
     * @param rightOperand The right operand that this operation works on.
     */
    protected BasicOperator(Calculable leftOperand, Calculable rightOperand) {
        _leftOperand = leftOperand;
        _rightOperand = rightOperand;
    }

    /**
     * The method returns the left operand for this operator.
     * @return The left operand for this operator.
     */
    public Calculable getLeftOperand() {
        return _leftOperand;
    }

    /**
     * The method returns the right operand for this operator.
     * @return The right operand for this operator.
     */
    public Calculable getRightOperand() {
        return _rightOperand;
    }

    @Override
    public String toString() {
        Calculable leftOperand = getLeftOperand();
        Calculable rightOperand = getRightOperand();

        String result = leftOperand + " " + getIdentifier() + " " + rightOperand;
        return result;
    }

}

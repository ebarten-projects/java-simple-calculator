package com.ebarten.calculator.calculables.operators;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.Operation;

/**
 * Created by Eran on 1/04/2017.
 */
public class Plus extends BasicOperator{

    /**
     * The constructor of this class.
     *
     * @param leftOperand  The left operand that this operation works on.
     * @param rightOperand The right operand that this operation works on.
     */
    public Plus(Calculable leftOperand, Calculable rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public double calculate() {
        Calculable leftOperand = getLeftOperand();
        Calculable rightOperand = getRightOperand();

        double leftOperandValue = leftOperand.calculate();
        double rightOperandValue = rightOperand.calculate();

        double result = leftOperandValue + rightOperandValue;

        return result;
    }

    @Override
    public String getIdentifier() {
        return "+";
    }

}

package com.ebarten.calculator.calculables.function;

import com.ebarten.calculator.calculables.Calculable;

/**
 * The class represents a cosine function.
 */
public class Cosine extends Function {
    /**
     * The constructor of this class.
     *
     * @param argument The argument to apply this function on.
     */
    protected Cosine(Calculable argument) {
        super(argument);
    }

    @Override
    public double calculate() {
        Calculable argument = getArgument();
        double result = argument.calculate();
        result = Math.cos(result);
        return result;
    }

    @Override
    public String getIdentifier() {
        return "cos";
    }

    @Override
    public String toString() {
        return getIdentifier() + "(" + getArgument().toString() + ")";
    }
}

package com.ebarten.calculator.calculables.function;

import com.ebarten.calculator.calculables.Calculable;
import com.ebarten.calculator.calculables.Operation;

/**
 * The class represents an arithmetic function that applies to an argument.
 */
public abstract class Function implements Operation {

    private Calculable _argument;

    /**
     * The constructor of this class.
     * @param argument The argument to apply this function on.
     */
    protected Function(Calculable argument) {
        _argument = argument;
    }

    /**
     * The method returns the argument that this function will be applied on.
     * @return The argument that this function will be applied on.
     */
    protected Calculable getArgument() {
        return _argument;
    }
}

package com.ebarten.calculator.calculables;

/**
 * The class represents an arithmetic unit that could be calculated.
 */
public interface Calculable {

    /**
     * The method returns the calculated result of this calculable.
     * @return The calculated result of this calculable.
     */
    public double calculate();

}

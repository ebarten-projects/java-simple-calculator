package com.ebarten.calculator.calculables;

/**
 * The class implements a Number calculable.
 */
public class Number implements Calculable {
    private double _value = 0;

    public Number(double value) {
        _value = value;
    }

    @Override
    public double calculate() {
        return _value;
    }

    @Override
    public String toString() {
        return "" + calculate();
    }
}

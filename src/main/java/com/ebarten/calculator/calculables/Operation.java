package com.ebarten.calculator.calculables;

/**
 * The class represents a generic operation who's result could be calculated.
 */
public interface Operation extends Calculable {
    /**
     * The method returns the identifier of this calculable.
     * @return The identifier of this calculable.
     */
    public String getIdentifier();

}
